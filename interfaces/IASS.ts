export interface IASS {

    selectionSort(array: Array<number>): Array<number>;

    insertionSort(array: Array<number>): Array<number>;

    mergeSort(array: Array<number>): Array<number>;

    bucketSort(arr: number[], bucketSize: number): number[];

    quickSort(arr: number[]): number[];

    heapSort(arr: number[]): number[];

    getArray(): Array<number>;
}