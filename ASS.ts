import { IASS } from "./interfaces/IASS";

// Algorithm structure sorting
export class ASS implements IASS {

    private array: Array<number> = [];

    constructor(count: number) {
        this.array = Array.from({length: count}, () => Math.floor(Math.random() * (10000 + 10000 + 1)) - 10000);
    }

    public getArray(): Array<number> {
        return this.array
    }

    public selectionSort(array: Array<number>): Array<number> {

        for (let i: number = 0; i < array.length; i++) {
            let min: number = i;

            for (let j: number = i + 1; j < array.length; j++) {
                if (array[j] < array[min]) {
                    min = j;
                }
            }

            [array[i], array[min]] = [array[min], array[i]];
        }

        return array;
    };


    public insertionSort(array: Array<number>): Array<number> {

        for (let i = 1; i < array.length; i++) {
            let current: number = array[i],
                j: number = i - 1;

            while ((j > -1) && (current < array[j])) {
                array[j + 1] = array[j];
                j--;
            }
            array[j + 1] = current;
        }

        return array;
    }

    public mergeSort(array: Array<number>): Array<number> {
        const half = array.length / 2

        if (array.length < 2) {
            return array
        }

        const left: Array<number> = array.splice(0, half)

        return this.merge(this.mergeSort(left), this.mergeSort(array))
    }

    private merge(left: Array<number>, right: Array<number>): Array<number> {
        let arr: Array<number> = [];
        let tmp: number | undefined;

        while (left.length && right.length) {
            tmp = left[0] < right[0]
                ? left.shift()
                : right.shift();

            tmp ? arr.push(tmp) : null
        }

        return [...arr, ...left, ...right]
    }

    public bucketSort(arr: number[], bucketSize: number = 5): number[] {
        if (arr.length === 0) return arr;

        const minValue = this.selfMin(arr);
        const maxValue = this.selfMax(arr);

        const bucketCount = Math.floor((maxValue - minValue) / bucketSize) + 1;
        const buckets: number[][] = new Array(bucketCount).fill(null).map(() => []);

        arr.forEach((num) => {
            const bucketIndex = Math.abs(Math.floor((num - minValue) / bucketSize));
            buckets[bucketIndex].push(num);
        });

        arr = [];
        buckets.forEach((bucket) => {
            const sortedBucket = this.quickSort(bucket);
            arr.push(...sortedBucket);
        });

        return arr;
    }

    public quickSort(array: number[]): number[] {
        if (array.length <= 1) return array;

        const pivotIndex: number = Math.floor(Math.random() * array.length);
        const pivotValue: number = array[pivotIndex];

        const left: number[] = [];
        const right: number[] = [];

        for (let i = 0; i < array.length; i++) {
            if (i === pivotIndex) continue;
            if (array[i] < pivotValue) left.push(array[i]);
            else right.push(array[i]);
        }

        return [...this.quickSort(left), pivotValue, ...this.quickSort(right)];
    }

    public heapSort(array: number[]): number[] {
        this.buildMaxHeap(array);

        for (let i = array.length - 1; i > 0; i--) {
            [array[0], array[i]] = [array[i], array[0]]
            this.heapify(array, 0, i);
        }

        return array;
    }

    private buildMaxHeap(array: number[]): void {
        const parentIndex = Math.floor((array.length - 1) / 2);

        for (let i = parentIndex; i >= 0; i--) {
            this.heapify(array, i, array.length);
        }
    }

    private heapify(array: number[], index: number, heapSize: number): void {
        const leftChildIndex = index * 2 + 1;
        const rightChildIndex = index * 2 + 2;
        let maxIndex = index;

        if (leftChildIndex < heapSize && array[leftChildIndex] > array[maxIndex]) {
            maxIndex = leftChildIndex;
        }

        if (rightChildIndex < heapSize && array[rightChildIndex] > array[maxIndex]) {
            maxIndex = rightChildIndex;
        }

        if (maxIndex !== index) {
            [array[index], array[maxIndex]] = [array[maxIndex], array[index]]
            this.heapify(array, maxIndex, heapSize);
        }
    }

    private selfMax(array: Array<number>) {
        let arrayTmp = [...array];
        let max = -Infinity;
        let arr = array.length / 10 * 9;
        
        for (let i = 0; i < 10; i++) {
            let tmp = arrayTmp.splice(arr, array.length / 10);
            if ( max < Math.max(...tmp) )
                max = Math.max(...tmp);

            arr -= (array.length / 10 || 0);
        }

        return max;
    }

    private selfMin(array: Array<number>) {
        let arrayTmp = [...array];
        let min = Infinity;
        let arr = array.length - array.length / 10;
        
        for (let i = 0; i < 10; i++) {
            let tmp = arrayTmp.splice(arr, array.length / 10);
            if ( min > Math.min(...tmp) )
                min = Math.min(...tmp);

            arr -= array.length / 10;
        }

        return min;
    }
}