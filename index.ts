import { ASS } from "./ASS";

for (let j = 2; j <= 5; j++) {
    for (let i = 1; i <= 9; i++) {
        const ass = new ASS(i * Math.pow(10, j)); // new Object in Algorithm structure sorting
        const arr = ass.getArray();
        const arrMethods = [
            'bucketSort',
            'heapSort',
            'insertionSort',
            'quickSort',
            'selectionSort',
            'mergeSort'
        ]

        let timeArr: Array<number> = []

        arrMethods.forEach((method: string) => {
            let timeStart = Date.now()
            eval(`ass['${method}'](arr)`)
            let timeEnd = Date.now()
            timeArr.push(timeEnd - timeStart);
        })

        // let timeStart = Date.now()
        // arr.sort((a, b) => a - b)
        // let timeEnd = Date.now()
        // timeArr.push(timeEnd - timeStart);

        console.log(
`
bucketSort                  => ${timeArr[0]} ms  
heapSort                    => ${timeArr[1]} ms 
insertionSort               => ${timeArr[2]} ms
quickSort                   => ${timeArr[3]} ms
selectionSort               => ${timeArr[4]} ms
mergeSort                   => ${timeArr[5]} ms
array.sort((a, b) => a - b) => ${timeArr[6]} ms
`
        );
        console.log('count - ', i * Math.pow(10, j));
        console.log('-------------------------------');
    }
}