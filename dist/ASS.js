"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ASS = void 0;
// Algorithm structure sorting
class ASS {
    constructor(count) {
        this.array = [];
        this.array = Array.from({ length: count }, () => Math.floor(Math.random() * (10000 + 10000 + 1)) - 10000);
    }
    getArray() {
        return this.array;
    }
    selectionSort(array) {
        for (let i = 0; i < array.length; i++) {
            let min = i;
            for (let j = i + 1; j < array.length; j++) {
                if (array[j] < array[min]) {
                    min = j;
                }
            }
            [array[i], array[min]] = [array[min], array[i]];
        }
        return array;
    }
    ;
    insertionSort(array) {
        for (let i = 1; i < array.length; i++) {
            let current = array[i], j = i - 1;
            while ((j > -1) && (current < array[j])) {
                array[j + 1] = array[j];
                j--;
            }
            array[j + 1] = current;
        }
        return array;
    }
    mergeSort(array) {
        const half = array.length / 2;
        if (array.length < 2) {
            return array;
        }
        const left = array.splice(0, half);
        return this.merge(this.mergeSort(left), this.mergeSort(array));
    }
    merge(left, right) {
        let arr = [];
        let tmp;
        while (left.length && right.length) {
            tmp = left[0] < right[0]
                ? left.shift()
                : right.shift();
            tmp ? arr.push(tmp) : null;
        }
        return [...arr, ...left, ...right];
    }
    bucketSort(arr, bucketSize = 5) {
        if (arr.length === 0)
            return arr;
        const minValue = this.selfMin(arr);
        const maxValue = this.selfMax(arr);
        const bucketCount = Math.floor((maxValue - minValue) / bucketSize) + 1;
        const buckets = new Array(bucketCount).fill(null).map(() => []);
        arr.forEach((num) => {
            const bucketIndex = Math.abs(Math.floor((num - minValue) / bucketSize));
            buckets[bucketIndex].push(num);
        });
        arr = [];
        buckets.forEach((bucket) => {
            const sortedBucket = this.quickSort(bucket);
            arr.push(...sortedBucket);
        });
        return arr;
    }
    quickSort(array) {
        if (array.length <= 1)
            return array;
        const pivotIndex = Math.floor(Math.random() * array.length);
        const pivotValue = array[pivotIndex];
        const left = [];
        const right = [];
        for (let i = 0; i < array.length; i++) {
            if (i === pivotIndex)
                continue;
            if (array[i] < pivotValue)
                left.push(array[i]);
            else
                right.push(array[i]);
        }
        return [...this.quickSort(left), pivotValue, ...this.quickSort(right)];
    }
    heapSort(array) {
        this.buildMaxHeap(array);
        for (let i = array.length - 1; i > 0; i--) {
            [array[0], array[i]] = [array[i], array[0]];
            this.heapify(array, 0, i);
        }
        return array;
    }
    buildMaxHeap(array) {
        const parentIndex = Math.floor((array.length - 1) / 2);
        for (let i = parentIndex; i >= 0; i--) {
            this.heapify(array, i, array.length);
        }
    }
    heapify(array, index, heapSize) {
        const leftChildIndex = index * 2 + 1;
        const rightChildIndex = index * 2 + 2;
        let maxIndex = index;
        if (leftChildIndex < heapSize && array[leftChildIndex] > array[maxIndex]) {
            maxIndex = leftChildIndex;
        }
        if (rightChildIndex < heapSize && array[rightChildIndex] > array[maxIndex]) {
            maxIndex = rightChildIndex;
        }
        if (maxIndex !== index) {
            [array[index], array[maxIndex]] = [array[maxIndex], array[index]];
            this.heapify(array, maxIndex, heapSize);
        }
    }
    selfMax(array) {
        let arrayTmp = [...array];
        let max = -Infinity;
        let arr = array.length / 10 * 9;
        for (let i = 0; i < 10; i++) {
            let tmp = arrayTmp.splice(arr, array.length / 10);
            if (max < Math.max(...tmp))
                max = Math.max(...tmp);
            arr -= (array.length / 10 || 0);
        }
        return max;
    }
    selfMin(array) {
        let arrayTmp = [...array];
        let min = Infinity;
        let arr = array.length - array.length / 10;
        for (let i = 0; i < 10; i++) {
            let tmp = arrayTmp.splice(arr, array.length / 10);
            if (min > Math.min(...tmp))
                min = Math.min(...tmp);
            arr -= array.length / 10;
        }
        return min;
    }
}
exports.ASS = ASS;
//# sourceMappingURL=ASS.js.map